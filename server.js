const redis = require('redis');
const app = require('fastify')();
const config = require('./config');
const redis_se = redis.createClient({port:config.REDIS_PORT,host:config.REDIS_URL}); // this creates a new client
const port = process.env.PORT || config.NS_DEFAULTPORT;

// app.register(require('fastify-redis'), {host:config.REDIS_URL,port:config.REDIS_PORT});
// check connection with Redis
redis_se.on('connect', () => {
    console.log('Redis client connection successful');
});

//API testing
app.get("/", (req,res,next) => {
    res.send('Enter API System');
});

app.get("/vote/:score/:userid/:postid", (req,res,next) => {
    try{
        if(!isNaN(parseInt(req.params.userid))&&!isNaN(parseInt(req.params.postid))&&!isNaN(parseInt(req.params.score))){
            parseInt(req.params.score) === 1 ? redis_se.incr(config.POSTID+parseInt(req.params.postid)+"_UPVOTE") : redis_se.incr(config.POSTID+parseInt(req.params.postid)+"_DOWNVOTE");
            redis_se.rpush(parseInt(req.params.userid),JSON.stringify({"postid" : parseInt(req.params.postid),"score" : parseInt(req.params.score)}))
        }
        return res.send({"success":true})
    }
    catch (err){
        res.statusCode = 500;
        return res.send({"success":false,"err":err})
    }
});


app.get("/vote-count/:postid", (req,res,next) => {
    try{
        let upvote=0,downvote=0;
        redis_se.get(config.POSTID+parseInt(req.params.postid)+"_UPVOTE",function(err,value){
            value != null ? upvote = parseInt(value) : upvote = 0;
            redis_se.get(config.POSTID+parseInt(req.params.postid)+"_DOWNVOTE",function (err,value) {
                value != null ? downvote = parseInt(value) : downvote = 0;
                return res.send({"upvote":upvote,"downvote":downvote})
            });
        });
    }catch (err) {
        res.statusCode = 500;
        return res.send({"success":false,"err":err})
    }
});

app.get("/vote-status/:userid", (req,res,next)=> {
    let list = [];
    redis_se.lrange(parseInt(req.params.userid), 0, -1 ,function (err,value) {
        value.forEach(function (item) {
            list.push(JSON.parse(item));
        });
        console.log(list);
        return res.send(list);
    });
});


app.listen(port);
console.log(`Server running: listen port - ${port}`);