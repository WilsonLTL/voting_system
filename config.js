let config = {};
config.REDIS_URL = "3.0.95.185";
config.REDIS_PORT = 6379;
config.NS_DEFAULTPORT = 3000;
config.POSTID = "POSTID";

config.ERROR_404_en = "Error 404 - request fail, Please check your link";
config.ERROR_400_en = "Error 400 - Please double check your format";
config.ERROR_405_en = "Error 405 - Please double check your request format"
module.exports = config;